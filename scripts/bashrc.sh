# This script is run on every interactive bash call (when the user is
# already logged in).
#
# Copyright (C) 2020-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.





# Shell customizations
#
# Note that unlike variables, functions and aliases are not passed to
# sub-processes (sub-shells), so we'll have to ask the shell to define
# them every time (even if they are loaded by bash_profile).
source ~/.bash_custom_dir/bash-customize.sh

# If you need any other startup commands outside of the ones in this
# project, build a '~/bash_custom_private_dir' and load the scripts.
if [ -d ~/.bash_custom_private_dir ]; then
    source ~/.bash_custom_private_dir/bash-customize.sh
fi



# `.bash_profile' also loads `.bashrc', we don't want to redefine these.
if [ x$BASHRC_READ = x ]; then

    # Set the variable to avoid adding these.
    export BASHRC_READ=1

    # Set the environment to English
    export LANG=en_US.utf8

    # Include my own paths
    export PATH="/home/$(whoami)/.local/bin:$PATH"
    export LDFLAGS="-L/home/$(whoami)/.local/lib $LDFLAGS"
    export MANPATH="/home/$(whoami)/.local/share/man/:$MANPATH"
    export CPPFLAGS="-I/home/$(whoami)/.local/include $CPPFLAGS"
    export INFOPATH="/home/$(whoami)/.local/share/info/:$INFOPATH"
    export LD_LIBRARY_PATH="/home/$(whoami)/.local/lib:$LD_LIBRARY_PATH"

    # To use CVS over SSH.
    export CVS_RSH=ssh
fi
